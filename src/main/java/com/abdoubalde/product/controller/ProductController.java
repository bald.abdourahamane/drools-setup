package com.abdoubalde.product.controller;

import com.abdoubalde.product.model.Product;
import com.abdoubalde.utils.SessionConstants;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    private final KieContainer kieContainer;

    public ProductController(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    /**
     * see rule execution in logs
     **/
    @PostMapping("/stock")
    public void stock(@RequestBody Product product) {
        KieSession kieSession = kieContainer.newKieSession(SessionConstants.STOCK_SESSION);
        kieSession.insert(product);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
