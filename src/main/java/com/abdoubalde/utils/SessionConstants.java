package com.abdoubalde.utils;

public class SessionConstants {
    public static final String DISCOUNT_SESSION = "discountKieSession";
    public static final String STOCK_SESSION = "stockKieSession";
}
