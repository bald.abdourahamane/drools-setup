package com.abdoubalde.person.controller;

import com.abdoubalde.person.model.Person;
import com.abdoubalde.utils.SessionConstants;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    private final KieContainer kieContainer;

    public PersonController(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    /**
     * see rule execution in logs
     * */
    @PostMapping("/discount")
    public void discount(@RequestBody Person person) {
        KieSession kieSession = kieContainer.newKieSession(SessionConstants.DISCOUNT_SESSION);
        kieSession.insert(person);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
