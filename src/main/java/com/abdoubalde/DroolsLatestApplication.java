package com.abdoubalde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsLatestApplication {
    public static void main(String[] args) {
        SpringApplication.run(DroolsLatestApplication.class, args);

    }

}
